program Matrix;

{$APPTYPE CONSOLE}

uses
  SysUtils;

type
  TMatrix = array[1..3, 1..3] of Integer;

const
  A: TMatrix = ((1, 4, 2), (2, 1, -2), (0, 1, -1));
  B: TMatrix = ((4, 6, -2), (4, 10, 1), (2, 4, -5));
  C: TMatrix = ((1, 0, 0), (0, 1, 0), (0, 0, 1));


var
  M: TMatrix;    //Result;
  O, T: Integer; //Counters;

//Function of addition:
function Summ(X, Y: TMatrix): TMatrix;
Var
  I, J: Integer;
begin
  For I := 1 to 3 do
  begin
    For J := 1 to 3 do
    begin
      X[I, J] := X[I, J] + Y[I, J];
    end;
  end;

  Result := X;
end;

//Function of Substracting:
function Substract(X, Y: TMatrix): TMatrix;
Var
  I, J: Integer;
begin
  For I := 1 to 3 do
  begin
    For J := 1 to 3 do
    begin
      X[I, J] := X[I, J] - Y[I, J];
    end;
  end;

  Result := X;
end;

//Function of Multiplication:
function Multiplication(X, Y: TMatrix): TMatrix;
var
  I, J, K: Integer;
  R: TMatrix;
begin
   R := C;
   for I := 1 to 3 do
   begin
     for J := 1 to 3 do
     begin
       R[I, J] := 0;

       For K := 1 to 3 do
       begin
         R[I, J] := R[I, J] + X[I, K] * Y[K, J];
       end;
     end;
   end;

   Result := R;
end;

//Function Int to TMatrix:
function MultConst(A: Integer): TMatrix;
var
  P: TMatrix;
  I: Integer;
begin
  P := C;

  For I := 1 to 3 do
  begin
    P[I, I] := P[I, I] * A;
  end;

  Result := P;
end;

//Our Formula:
function Formula(X, Y: TMatrix): TMatrix;
var
  P, F: TMatrix;
begin
  P := Multiplication(X, X);  // = A^2
  P := Summ(P, Y);            // = A^2 + B
  P := Multiplication(P, Y);  // = (A^2 + B) * B
  F := MultConst(2);          // = 2 * E, E = ((1, 0, 0), (0, 1, 0), (0, 0, 1))
  F := Multiplication(F, A);  // = 2 * E * A
  Result := Substract(F, P);  // = 2 * E * A - (A^2 + B) * B
end;

begin
  M := Formula(A, B);

  For O := 1 to 3 do
  begin
    For T := 1 to 3 do
    begin
      write(M[O, T], '  ');
    end;
    Writeln(#13#10);
  end;
  Readln;
end.
